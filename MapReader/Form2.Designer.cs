﻿namespace WindowsFormsApplication2
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAddRow = new System.Windows.Forms.Button();
            this.dataGridObj = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnWriteXmlFile = new System.Windows.Forms.Button();
            this.btnLoadMapFile = new System.Windows.Forms.Button();
            this.btnLoadXmlFile = new System.Windows.Forms.Button();
            this.btnGenVariables = new System.Windows.Forms.Button();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.txtBoxPath = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridObj)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddRow
            // 
            this.btnAddRow.Location = new System.Drawing.Point(710, 664);
            this.btnAddRow.Name = "btnAddRow";
            this.btnAddRow.Size = new System.Drawing.Size(106, 37);
            this.btnAddRow.TabIndex = 0;
            this.btnAddRow.Text = "Add Row";
            this.btnAddRow.UseVisualStyleBackColor = true;
            this.btnAddRow.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridObj
            // 
            this.dataGridObj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridObj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dataGridObj.Location = new System.Drawing.Point(12, 136);
            this.dataGridObj.Name = "dataGridObj";
            this.dataGridObj.RowTemplate.Height = 24;
            this.dataGridObj.Size = new System.Drawing.Size(692, 565);
            this.dataGridObj.TabIndex = 2;
            this.dataGridObj.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridObj_CellContentClick);
            this.dataGridObj.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dataGridObj_KeyDown);
            this.dataGridObj.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridObj_MouseClick);
            // 
            // Column1
            // 
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column1.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column1.HeaderText = "No";
            this.Column1.Name = "Column1";
            this.Column1.Width = 50;
            // 
            // Column2
            // 
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column2.DefaultCellStyle = dataGridViewCellStyle6;
            this.Column2.HeaderText = "Name";
            this.Column2.Name = "Column2";
            this.Column2.Width = 200;
            // 
            // Column3
            // 
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column3.DefaultCellStyle = dataGridViewCellStyle7;
            this.Column3.HeaderText = "Address[Hex]";
            this.Column3.Name = "Column3";
            this.Column3.Width = 200;
            // 
            // Column4
            // 
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column4.DefaultCellStyle = dataGridViewCellStyle8;
            this.Column4.HeaderText = "Data Type";
            this.Column4.Name = "Column4";
            this.Column4.Width = 200;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBar1,
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 704);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(897, 25);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(100, 19);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(151, 20);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // btnWriteXmlFile
            // 
            this.btnWriteXmlFile.Location = new System.Drawing.Point(705, 41);
            this.btnWriteXmlFile.Name = "btnWriteXmlFile";
            this.btnWriteXmlFile.Size = new System.Drawing.Size(169, 39);
            this.btnWriteXmlFile.TabIndex = 4;
            this.btnWriteXmlFile.Text = "Write Xml File";
            this.btnWriteXmlFile.UseVisualStyleBackColor = true;
            this.btnWriteXmlFile.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnLoadMapFile
            // 
            this.btnLoadMapFile.Location = new System.Drawing.Point(187, 91);
            this.btnLoadMapFile.Name = "btnLoadMapFile";
            this.btnLoadMapFile.Size = new System.Drawing.Size(169, 39);
            this.btnLoadMapFile.TabIndex = 6;
            this.btnLoadMapFile.Text = "Load .map File";
            this.btnLoadMapFile.UseVisualStyleBackColor = true;
            this.btnLoadMapFile.Click += new System.EventHandler(this.btnLoadFile_Click);
            // 
            // btnLoadXmlFile
            // 
            this.btnLoadXmlFile.Location = new System.Drawing.Point(705, 91);
            this.btnLoadXmlFile.Name = "btnLoadXmlFile";
            this.btnLoadXmlFile.Size = new System.Drawing.Size(169, 39);
            this.btnLoadXmlFile.TabIndex = 7;
            this.btnLoadXmlFile.Text = "Load Xml File";
            this.btnLoadXmlFile.UseVisualStyleBackColor = true;
            this.btnLoadXmlFile.Click += new System.EventHandler(this.btnLoadXmlFile_Click);
            // 
            // btnGenVariables
            // 
            this.btnGenVariables.Location = new System.Drawing.Point(12, 91);
            this.btnGenVariables.Name = "btnGenVariables";
            this.btnGenVariables.Size = new System.Drawing.Size(155, 39);
            this.btnGenVariables.TabIndex = 8;
            this.btnGenVariables.Text = "Generate Variables";
            this.btnGenVariables.UseVisualStyleBackColor = true;
            this.btnGenVariables.Click += new System.EventHandler(this.btnGenVariables_Click);
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(593, 7);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(96, 32);
            this.btnBrowse.TabIndex = 9;
            this.btnBrowse.Text = "Browse";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // txtBoxPath
            // 
            this.txtBoxPath.Location = new System.Drawing.Point(12, 12);
            this.txtBoxPath.Name = "txtBoxPath";
            this.txtBoxPath.Size = new System.Drawing.Size(565, 22);
            this.txtBoxPath.TabIndex = 10;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 729);
            this.Controls.Add(this.txtBoxPath);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.btnGenVariables);
            this.Controls.Add(this.btnLoadXmlFile);
            this.Controls.Add(this.btnLoadMapFile);
            this.Controls.Add(this.btnWriteXmlFile);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridObj);
            this.Controls.Add(this.btnAddRow);
            this.Name = "Form2";
            this.Text = "Form2";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form2_FormClosed);
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridObj)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAddRow;
        private System.Windows.Forms.DataGridView dataGridObj;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnWriteXmlFile;
        private System.Windows.Forms.Button btnLoadMapFile;
        private System.Windows.Forms.Button btnLoadXmlFile;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.Button btnGenVariables;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.TextBox txtBoxPath;
    }
}