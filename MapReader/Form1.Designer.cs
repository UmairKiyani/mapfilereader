﻿namespace WindowsFormsApplication2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBoxServer = new System.Windows.Forms.GroupBox();
            this.button11 = new System.Windows.Forms.Button();
            this.groupBoxLocal = new System.Windows.Forms.GroupBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBoxQuick = new System.Windows.Forms.GroupBox();
            this.btnCSK = new System.Windows.Forms.Button();
            this.btnBrandXlcl = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.groupBoxServer.SuspendLayout();
            this.groupBoxLocal.SuspendLayout();
            this.groupBoxQuick.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(6, 27);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(147, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "L1000 Documents";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(6, 21);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(147, 35);
            this.button2.TabIndex = 1;
            this.button2.Text = "Manuals";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBoxServer
            // 
            this.groupBoxServer.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBoxServer.Controls.Add(this.button11);
            this.groupBoxServer.Controls.Add(this.button1);
            this.groupBoxServer.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxServer.Location = new System.Drawing.Point(12, 12);
            this.groupBoxServer.Name = "groupBoxServer";
            this.groupBoxServer.Size = new System.Drawing.Size(288, 135);
            this.groupBoxServer.TabIndex = 2;
            this.groupBoxServer.TabStop = false;
            this.groupBoxServer.Text = "Server";
            this.groupBoxServer.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button11
            // 
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.Location = new System.Drawing.Point(6, 68);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(147, 35);
            this.button11.TabIndex = 1;
            this.button11.Text = "Brand X";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // groupBoxLocal
            // 
            this.groupBoxLocal.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBoxLocal.Controls.Add(this.button7);
            this.groupBoxLocal.Controls.Add(this.button4);
            this.groupBoxLocal.Controls.Add(this.button3);
            this.groupBoxLocal.Controls.Add(this.button2);
            this.groupBoxLocal.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxLocal.Location = new System.Drawing.Point(12, 163);
            this.groupBoxLocal.Name = "groupBoxLocal";
            this.groupBoxLocal.Size = new System.Drawing.Size(288, 213);
            this.groupBoxLocal.TabIndex = 3;
            this.groupBoxLocal.TabStop = false;
            this.groupBoxLocal.Text = "Local";
            // 
            // button7
            // 
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.Location = new System.Drawing.Point(6, 144);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(147, 35);
            this.button7.TabIndex = 4;
            this.button7.Text = "DCP4 Test Report";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(6, 103);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(147, 35);
            this.button4.TabIndex = 3;
            this.button4.Text = "GEM Code";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(6, 62);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(147, 35);
            this.button3.TabIndex = 2;
            this.button3.Text = "Cosmos Code";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBoxQuick
            // 
            this.groupBoxQuick.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBoxQuick.Controls.Add(this.btnCSK);
            this.groupBoxQuick.Controls.Add(this.btnBrandXlcl);
            this.groupBoxQuick.Controls.Add(this.button9);
            this.groupBoxQuick.Controls.Add(this.button6);
            this.groupBoxQuick.Controls.Add(this.button5);
            this.groupBoxQuick.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Bold);
            this.groupBoxQuick.Location = new System.Drawing.Point(318, 12);
            this.groupBoxQuick.Name = "groupBoxQuick";
            this.groupBoxQuick.Size = new System.Drawing.Size(172, 364);
            this.groupBoxQuick.TabIndex = 4;
            this.groupBoxQuick.TabStop = false;
            this.groupBoxQuick.Text = "Quick";
            this.groupBoxQuick.Enter += new System.EventHandler(this.groupBox1_Enter_1);
            // 
            // btnCSK
            // 
            this.btnCSK.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCSK.Location = new System.Drawing.Point(6, 266);
            this.btnCSK.Name = "btnCSK";
            this.btnCSK.Size = new System.Drawing.Size(147, 35);
            this.btnCSK.TabIndex = 6;
            this.btnCSK.Text = "CSK";
            this.btnCSK.UseVisualStyleBackColor = true;
            this.btnCSK.Click += new System.EventHandler(this.btnCSK_Click);
            // 
            // btnBrandXlcl
            // 
            this.btnBrandXlcl.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBrandXlcl.Location = new System.Drawing.Point(6, 213);
            this.btnBrandXlcl.Name = "btnBrandXlcl";
            this.btnBrandXlcl.Size = new System.Drawing.Size(147, 35);
            this.btnBrandXlcl.TabIndex = 5;
            this.btnBrandXlcl.Text = "Brand X";
            this.btnBrandXlcl.UseVisualStyleBackColor = true;
            this.btnBrandXlcl.Click += new System.EventHandler(this.btnBrandXlcl_Click);
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Location = new System.Drawing.Point(6, 37);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(147, 35);
            this.button9.TabIndex = 4;
            this.button9.Text = "Desktop";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button6
            // 
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.Location = new System.Drawing.Point(6, 151);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(147, 35);
            this.button6.TabIndex = 3;
            this.button6.Text = "DCP4 L1000A";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Khaki;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(6, 91);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(147, 35);
            this.button5.TabIndex = 2;
            this.button5.Text = "DCP4 LA700";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(18, 392);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 28);
            this.button8.TabIndex = 5;
            this.button8.Text = "Next";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold);
            this.button10.Location = new System.Drawing.Point(568, 100);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(100, 38);
            this.button10.TabIndex = 6;
            this.button10.Text = "Flash Tool";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(749, 432);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.groupBoxQuick);
            this.Controls.Add(this.groupBoxLocal);
            this.Controls.Add(this.groupBoxServer);
            this.Name = "Form1";
            this.Text = "OpenFolder";
            this.groupBoxServer.ResumeLayout(false);
            this.groupBoxLocal.ResumeLayout(false);
            this.groupBoxQuick.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBoxServer;
        private System.Windows.Forms.GroupBox groupBoxLocal;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBoxQuick;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button btnBrandXlcl;
        private System.Windows.Forms.Button btnCSK;
    }
}

