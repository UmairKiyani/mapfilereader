﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;


namespace WindowsFormsApplication2
{
   

    public partial class Form2 : Form
    {
    


        public Form2()
        {
            InitializeComponent();
            
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            int i = 0;
           
            for (; i < 25; i++)
            {
                dataGridObj.Rows.Add();
                dataGridObj.Rows[i].Cells[0].Value = i+1;

            }
            dataGridObj.Rows[i].Cells[0].Value = i + 1;



            dataGridObj.AllowUserToAddRows = false;


            txtBoxPath.Text = Properties.Settings.Default.path;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int index = dataGridObj.Rows.Count;

            //and count +1 to get a new row id:
            //index++;
            dataGridObj.Rows.Add();
            dataGridObj.Rows[dataGridObj.Rows.Count - 1].Cells[0].Value = index+1;
        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            WriteXmlFile();
        }

        private void cmbBoxPath_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnLoadFile_Click(object sender, EventArgs e)
        {
            string VarNameToBeSearched = string.Empty;
            object VarObjToBeSearched = null;
            int indexOfNameColumn = 1;
            int indexOfAddressColumn = 2;
            if (txtBoxPath.Text != String.Empty)
            {
                foreach (DataGridViewRow row in dataGridObj.Rows)
                {
                    VarObjToBeSearched = row.Cells[indexOfNameColumn].Value; //Get the Name
                    if (VarObjToBeSearched != null)
                    {
                        VarNameToBeSearched = VarObjToBeSearched.ToString();
                        VarNameToBeSearched = VarNameToBeSearched.Trim(); //Remove spaces from start and end

                        string[] lines = System.IO.File.ReadAllLines(txtBoxPath.Text);
                        foreach (string line in lines)
                        {
                            if (line.Contains(VarNameToBeSearched))
                            {
                                int startIndVar = line.IndexOf(VarNameToBeSearched);
                                int lastIndVar = VarNameToBeSearched.Length;
                                //next character after the variable name must be a " ", 
                                //so that other varible containing this variable are not selected
                                string emptySpace = line.Substring(startIndVar + lastIndVar, 1);
                                if (emptySpace.Equals(" "))
                                {
                                    string address = ParselineToFindAddress(line);
                                    row.Cells[indexOfAddressColumn].Value = address;
                      
                                }
                            }
                        }
                    }
                }
            }
        }


 //Function to Parse athe line and Take the address value out*/
 string ParselineToFindAddress(string line)
        {

            int Start, End;
            Start = line.IndexOf("0x0");
            End = Start + 10;
            string address = line.Substring(Start+2, End - Start);
            return address;
        }
void WriteXmlFile()
        {

            try
            {
                //Create a datatable to store XML data
                DataTable DT = new DataTable();
                DT.Columns.Add("Sr.No.");
                DT.Columns.Add("Name");
                DT.Columns.Add("Address");
                DT.Columns.Add("Data");
                foreach (DataGridViewRow row in dataGridObj.Rows)
                {
                  DT.Rows.Add(new object[] { row.Cells[0].Value, row.Cells[1].Value, row.Cells[2].Value, row.Cells[3].Value });
                }

                //Create a dataset
                DataSet DS = new DataSet();

                //Add datatable to this dataset
                DS.Tables.Add(DT);

                //Write dataset to XML file
                DS.WriteXml(@"C:\Umair\myFile.xml");

                MessageBox.Show("XML data written successfully to " + "txtXMLFilePath.Text");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
            }


        }

        private void btnLoadXmlFile_Click(object sender, EventArgs e)
        {
            try
            {
                //Initialize new Dataset
                DataSet DS = new DataSet();

                //Read XML data from file
                DS.ReadXml(@"C:\Umair\myFile.xml");

                foreach (DataGridViewRow row in dataGridObj.Rows)
                {
                    //assign values to cells in the new row
                    //row.Cells[0].Value = DS.Tables[0].Rows[row.Index]["No"];
                    row.Cells[1].Value = DS.Tables[0].Rows[row.Index]["Name"];
                    row.Cells[2].Value = DS.Tables[0].Rows[row.Index]["Address"];
                    //row.Cells[3].Value = DS.Tables[0].Rows[row.Index]["Data"];

                }

                dataGridObj.Refresh();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Exception: " + ex.Message);
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridObj_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {

                ContextMenu contextMenu = new ContextMenu();

                //m.MenuItems.Add(new MenuItem("Copy"));
                //m.MenuItems.Add(new MenuItem("Paste")); //Added
                //m.MenuItems.Add(new MenuItem("Delete")); //Added


                MenuItem menuItem = new MenuItem("Copy");
                menuItem.Click += new EventHandler(CopyAction);
                contextMenu.MenuItems.Add(menuItem);

                menuItem = new MenuItem("Paste");
                menuItem.Click += new EventHandler(PasteAction);
                contextMenu.MenuItems.Add(menuItem);

                menuItem = new MenuItem("Delete");
                menuItem.Click += new EventHandler(DeleteAction);
                contextMenu.MenuItems.Add(menuItem);

                int currentMouseOverRow = dataGridObj.HitTest(e.X, e.Y).RowIndex;
                contextMenu.Show(dataGridObj, new Point(e.X, e.Y));

  
            }


        }

//-----------------Copy-------------------------------------------------------------
        void CopyAction(object sender, EventArgs e)
        {
            CopyToClipboard();
        }

        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr GetOpenClipboardWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        private void CopyToClipboard()
        {
            if (dataGridObj.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                try
                {

                    //Check if other applications have opened clipboard
                    IntPtr hwnd = GetOpenClipboardWindow();
                    if (hwnd == IntPtr.Zero)
                    {
                        // Add the selection to the clipboard.
                        DataObject dataObj = dataGridObj.GetClipboardContent();
                        Clipboard.Clear();
                        if (dataObj != null)
                            Clipboard.SetDataObject(dataObj, false, 10, 300);

                    }

                    else
                    {
                        MessageBox.Show("Please copy again. Another App is using clipboard.");
                    }

                }
                catch (System.Runtime.InteropServices.ExternalException)
                {
                    MessageBox.Show("The Clipboard could not be accessed. Please try again.");
                }
            }
        }

        //-----------------Cut-------------------------------------------------------------*/
        void CutAction(object sender, EventArgs e)
        {
            //Copy to clipboard
            CopyToClipboard();
            //Clear selected cells
            foreach (DataGridViewCell dgvCell in dataGridObj.SelectedCells)
                dgvCell.Value = string.Empty;

        }

//-----------------Paste-------------------------------------------------------------*/
        void PasteAction(object sender, EventArgs e)
        {

            //Show Error if no cell is selected
            if (dataGridObj.SelectedCells.Count == 0)
            {
                MessageBox.Show("Please select a cell", "Paste",
                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //Get the starting Cell
            DataGridViewCell startCell = GetStartCell(dataGridObj);
            //Get the clipboard value in a dictionary
            Dictionary<int, Dictionary<int, string>> cbValue = ClipBoardValues(Clipboard.GetText());

            int iRowIndex = startCell.RowIndex;
            foreach (int rowKey in cbValue.Keys)
            {
                int iColIndex = startCell.ColumnIndex;
                foreach (int cellKey in cbValue[rowKey].Keys)
                {
                    //Check if the index is within the limit
                    if (iColIndex <= dataGridObj.Columns.Count - 1
                    && iRowIndex <= dataGridObj.Rows.Count - 1)
                    {
                        DataGridViewCell cell = dataGridObj[iColIndex, iRowIndex];

                        //Copy to selected cells if 'chkPasteToSelectedCells' is checked
                            cell.Value = cbValue[rowKey][cellKey];
                    }
                    iColIndex++;
                }
                iRowIndex++;
            }
        }
        private DataGridViewCell GetStartCell(DataGridView dgView)
        {
            //get the smallest row,column index
            if (dgView.SelectedCells.Count == 0)
                return null;

            int rowIndex = dgView.Rows.Count - 1;
            int colIndex = dgView.Columns.Count - 1;

            foreach (DataGridViewCell dgvCell in dgView.SelectedCells)
            {
                if (dgvCell.RowIndex < rowIndex)
                    rowIndex = dgvCell.RowIndex;
                if (dgvCell.ColumnIndex < colIndex)
                    colIndex = dgvCell.ColumnIndex;
            }

            return dgView[colIndex, rowIndex];
        }

        private Dictionary<int, Dictionary<int, string>> ClipBoardValues(string clipboardValue)
        {
            Dictionary<int, Dictionary<int, string>>
            copyValues = new Dictionary<int, Dictionary<int, string>>();

            String[] lines = clipboardValue.Split('\n');

            for (int i = 0; i <= lines.Length - 1; i++)
            {
                copyValues[i] = new Dictionary<int, string>();
                String[] lineContent = lines[i].Split('\t');

                //if an empty cell value copied, then set the dictionary with an empty string
                //else Set value to dictionary
                if (lineContent.Length == 0)
                    copyValues[i][0] = string.Empty;
                else
                {
                    for (int j = 0; j <= lineContent.Length - 1; j++)
                        copyValues[i][j] = lineContent[j];
                }
            }
            return copyValues;
        }


//-----------------Delete--------------------------------------------------------*/

        void DeleteAction(object sender, EventArgs e)
        {
            int rowIndex, colIndex;
            foreach (DataGridViewCell oneCell in dataGridObj.SelectedCells)
            {
                rowIndex = oneCell.RowIndex;
                colIndex = oneCell.ColumnIndex;
                dataGridObj.Rows[rowIndex].Cells[colIndex].Value = null;
            }

        }

//Keyboard Keys

        private void dataGridObj_KeyDown(object sender, KeyEventArgs e)
        {
            //Delete Key
            if (e.KeyData == Keys.Delete)
            {
                DeleteAction(sender, e);
            }
            //ctrl+c key
            else if (e.Control && e.KeyCode == Keys.C)
            {
                CopyAction(sender, e);
            }
            //ctrl+v key
            else if (e.Control && e.KeyCode == Keys.V)
            {
                PasteAction(sender, e);
            }
            //ctrl+x key
            else if (e.Control && e.KeyCode == Keys.X)
            {
                CutAction(sender, e);
            }
            else
            {
            }

        }
        private void dataGridObj_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        /// <summary>
        /// Take the variable name in the selected cell of Name column
        /// and fill the following columns with the incrementing variable
        /// name.eg., if variable name was DbVar1, then fill the next rows
        /// with DbVar2, DbVar3....DbVar9
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenVariables_Click(object sender, EventArgs e)
        {
            string selVarName; string nextVarName;
            object selVarEntry = null;
            int colIndex = dataGridObj.CurrentCell.ColumnIndex;
            int rowIndex = dataGridObj.CurrentRow.Index;
            selVarEntry = dataGridObj.Rows[rowIndex].Cells[colIndex].Value;
            if(selVarEntry != null)
            { 
                selVarName = selVarEntry.ToString();
                if (selVarName != string.Empty)
                {
                    selVarName = selVarName.Trim(); //Trim spaces from start and end
                    char lastChar = selVarName[selVarName.Length - 1];
                    int lastCharVal = (int)Char.GetNumericValue(lastChar);
                    if ((lastCharVal >= 1) && (lastCharVal <= 10)) //from 1 till 9
                    {
                        int CurrentValue = (int)Char.GetNumericValue(lastChar);
                        for (int j = lastCharVal; j < 10; j++)
                        {
                            CurrentValue++;
                            nextVarName = selVarName.Remove(selVarName.Length - 1, 1) + CurrentValue.ToString();
                            dataGridObj.Rows[++rowIndex].Cells[colIndex].Value = nextVarName;
                        }


                    }
                }

            }

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            System.IO.Stream myStream = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();


            openFileDialog1.Filter = "map files (*.map)|*.map";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFileDialog1.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            // Insert code to read the stream here.
                            string Path = openFileDialog1.FileName;
                            //txtBoxPath.Text = string.Format("{0}/{1}",System.IO.Path.GetDirectoryName(myStream), openFileDialog1.FileName);
                            txtBoxPath.Text = Path;
                            

                        }
                    }
                }
                catch (Exception ex)
                {
                   
                    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                }
            }
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.path = txtBoxPath.Text;



            Properties.Settings.Default.Save();
        }
    }
}

