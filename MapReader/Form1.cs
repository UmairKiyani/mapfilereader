﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("F:\\Inverter/Software Group/__Software Development Projects/L1000 Projects/L1000A Projects/L1000A+ DCP_CANopen-Lift Update (VSZ924798_VSA923402)");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("C:\\Users/Kiyani/Desktop/Manuals");
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("C:\\Umair/SourceCodes/COSMOS_Code"); 
        }

        private void button4_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("C:\\Umair/SourceCodes/GEM_Code");
        }

        private void groupBox1_Enter_1(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("C:\\Umair/SourceCodes/GEM_Code/LA700/DCP/withOptimization");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("C:\\Umair/SourceCodes/COSMOS_Code/L1000A/DCP4/WithOptimization");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("C://Umair/ProjectDocuments/DCP/Test");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            f2.ShowDialog();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("C:/Users/Kiyani/Desktop");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Program Files (x86)\Renesas Electronics\Programming Tools\Renesas Flash Programmer V2.05\RFP.exe");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"F:\Inverter\Software Group\_GEM Software");
        }

        private void btnBrandXlcl_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Umair\SourceCodes\GEM_Code\GA700\Q2A");
        }

        private void btnCSK_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Umair\ProjectDocuments\Q2A\Q2A Detailed Design (CSK)\Brand X\Doc\Spec\Detailed Design");
        }
    }
}
